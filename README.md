# Android Technical Task
Network Connection
In the source code you will get notification if you are not connected with Wifi Network or Mobile Data Network.

Internet Connection
Once you are connected with any of the aforementioned Networks, if there is no internet connection you will again get the notification of 
no Internet Connection.

Volley Library for API consuming (Volley 1.1.1)
The Request and responses are consumed by Volley Library to get the Details information. 

MVP Architecture
Model View Presenter is a clean user interface architechtural pattern, it ease the reusability code structure and responsible to provide
clean code. The MVP works in the background Thread other than the main Thread for UI perspective, which helps the user to use their
application without any lacking in the UI.

Pagination
The pagination has been done in both situation either online or offline. In Android Application concept we called it endless scrolling, 
you will get 20 items once and 20 more items when you are at the bottom of the list, and than 20 more will be added and so on. If there 
are no further List items available you will get notification of error or alert.

Google Maps

Use Google Map API for debug build into application. Visit google_maps_api.xml file under src > debug folder, and than find the link which
includes the SHA1 Key as well as the application Id or Generate SHA1 Key from Android Studio, when you get SHA1 Key. Just copy and past 
it into the Browser, you will be redirected to the Google Maps console. Generate Key and put it into the place of “YOUR_KEY_HERE”. Make 
sure you have to put your System SHA1.

OR

Google Maps API console, create the project over there, “Test_Project” you need to enter the SHA1 Key in the required field. Also copy the 
application Id from App Gradle in Android Studio if required, than put it in the package area of “Test_Project”. You are now able to 
generate the Key. Copy the KEYs and past it into the google_maps_api.xml at Android studio  for Google Maps.

Once you are done with these steps, you are now able to run the application and view the Google Maps after click on any Item.

Animation on Google Maps
When the Map loaded on the screen, you will see the details information of the Item. Touch the map to see the full view of the map and 
the details will be hided smoothly, and than touch again if you want to see the details again and the details will be shown as animated 
as describe. 


SQLite DB for Offline Cache Data
SQLite Local database has been used for cacheing the Data for offline loading. The data is inserted into the Data for the first load of 
the list, and will be inserted again once you load more data.  