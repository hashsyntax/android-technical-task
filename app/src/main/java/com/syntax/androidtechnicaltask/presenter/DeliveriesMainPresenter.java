package com.syntax.androidtechnicaltask.presenter;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.syntax.androidtechnicaltask.interfaces.VolleyResponseListener;
import com.syntax.androidtechnicaltask.model.DeliveriesResponse;
import com.syntax.androidtechnicaltask.utils.ConstantsUtils;
import com.syntax.androidtechnicaltask.utils.VolleyUtils;
import com.syntax.androidtechnicaltask.views.DeliveriesViewContractor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DeliveriesMainPresenter implements DeliveriesViewContractor.DeliveriesListPresenter {
    private static final String TAG = DeliveriesMainPresenter.class.getSimpleName();
    private DeliveriesViewContractor.ViewDeliveries viewDeliveries;
    private ObjectMapper objectMapper;

    public DeliveriesMainPresenter(DeliveriesViewContractor.ViewDeliveries viewDeliveries) {
        this.viewDeliveries = viewDeliveries;
    }
    /**
     * @USE_CASE Retrieve Delivery Items
     * */
    @Override
    public void requestDeliveries(int offset, int limit) {
        try {
            objectMapper = new ObjectMapper();
            viewDeliveries.onShowLoader();
            Log.d(TAG, "DELIVERIES_URL: " + ConstantsUtils.BASE_URL + ConstantsUtils.DELIVERIES_ENDPOINT + "offset=" + offset + "&limit=" + limit);
            VolleyUtils.getMethodJsonRequest(ConstantsUtils.BASE_URL + ConstantsUtils.DELIVERIES_ENDPOINT + "offset=" + offset + "&limit=" + limit, new VolleyResponseListener() {
                @Override
                public void onResponse(Object response) {
                    try {
                        Log.d(TAG, "DELIVERIES_RES: " + response.toString());
                        JSONArray jsonArray = objectToJSONArray(response);//objectMapper.readValue(response.toString(), JSONArray.class);
                        List<String> deliveriesList = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            try {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                deliveriesList.add(jsonObject.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        if (!deliveriesList.isEmpty()) {
                            viewDeliveries.onDeliveryList(getDeliveriesList(deliveriesList));
                        } else {
                            viewDeliveries.onError("Operation Failed!");
                        }

                    } catch (Exception e) {
                        viewDeliveries.onError("Operation Failed!");
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String message) {
                    viewDeliveries.onError(message);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Convert Object to JSONArray
     * @param object
     */
    private JSONArray objectToJSONArray(Object object) {
        Object json = null;
        JSONArray jsonArray = null;
        try {
            json = new JSONTokener(object.toString()).nextValue();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (json instanceof JSONArray) {
            jsonArray = (JSONArray) json;
        }
        return jsonArray;
    }

    /**
     * Extract the Deliveries Model Object (POJO)
     * @param deliveriesList
     */
    private List<DeliveriesResponse> getDeliveriesList(List<String> deliveriesList) {
        List<DeliveriesResponse> deliveriesResponseList = new ArrayList<>();
        try {
            for (String object : deliveriesList) {
                DeliveriesResponse deliveriesResponse = objectMapper.readValue(object, DeliveriesResponse.class);
                deliveriesResponseList.add(deliveriesResponse);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deliveriesResponseList;
    }
}
