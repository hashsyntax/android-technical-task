package com.syntax.androidtechnicaltask.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.syntax.androidtechnicaltask.database.handler.DbHandler;
import com.syntax.androidtechnicaltask.model.DeliveriesResponse;
import com.syntax.androidtechnicaltask.model.LocationLatLong;

import java.util.ArrayList;
import java.util.List;

public class DeliveryItemDAO {
    private static final String TAG = DeliveryItemDAO.class.getSimpleName();
    // Delivery Items Table
    public static final String DELIVERY_ITEMS_TABLE = "delivery_item_tbl";

    //Delivery Items: Columns
    public static final String TABLE_ID = "_id";
    public static final String ITEM_ID = "item_id";
    public static final String ITEM_DESCRIPTION = "item_description";
    public static final String ITEM_IMAGE_URL = "item_image";
    public static final String ITEM_LATITUDE = "item_latitude";
    public static final String ITEM_LONGITUDE = "item_longitude";
    public static final String ITEM_ADDRESS = "item_address";

    private DbHandler dbHandler;
    private SQLiteDatabase liteDatabase;

    public DeliveryItemDAO(Context context) {
        this.dbHandler = new DbHandler(context);
        this.liteDatabase = dbHandler.getWritableDatabase();
    }

    /**
     * @CACHE Delivery Items
     * Inserted All the rows fetched from the API
     */
    public void cacheDeliveryItems(DeliveriesResponse deliveriesResponse) {
        try {
            //Option DB connection to write Data
            SQLiteDatabase liteDatabase = dbHandler.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(ITEM_ID, deliveriesResponse.getItemId());
            values.put(ITEM_DESCRIPTION, deliveriesResponse.getDescription());
            values.put(ITEM_IMAGE_URL, deliveriesResponse.getImageUrl());
            values.put(ITEM_LATITUDE, deliveriesResponse.getLocation().getLat());
            values.put(ITEM_LONGITUDE, deliveriesResponse.getLocation().getLng());
            values.put(ITEM_ADDRESS, deliveriesResponse.getLocation().getAddress());
            Log.d(TAG, "ITEMS_RECEIVED");
            liteDatabase.insertWithOnConflict(DELIVERY_ITEMS_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            liteDatabase.close();/*Closing the DB*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @ROWS according to the Specified numbers
     * @param startNum
     * @param endNum
     * SQL Query is used (BETWEEN)
     * For Example, TABLE_ID BETWEEN 0 AND 20
     * */
    public List<DeliveriesResponse> getDeliveryItemsList(int startNum, int endNum) {
        List<DeliveriesResponse> modelList = new ArrayList<DeliveriesResponse>();
        String selectQuery = "SELECT * FROM " + DELIVERY_ITEMS_TABLE + " WHERE " + TABLE_ID + " BETWEEN " + startNum + " AND " + endNum;

        SQLiteDatabase db = dbHandler.getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    DeliveriesResponse model = new DeliveriesResponse();
                    LocationLatLong locationLatLong = new LocationLatLong();
                    model.setItemId(Integer.parseInt(cursor.getString(1)));
                    model.setDescription(cursor.getString(2));
                    model.setImageUrl(cursor.getString(3));
                    locationLatLong.setLat(cursor.getDouble(4));
                    locationLatLong.setLng(cursor.getDouble(5));
                    locationLatLong.setAddress(cursor.getString(6));
                    model.setLocation(locationLatLong);
                    modelList.add(model);
                } while (cursor.moveToNext());
            }
            return modelList;

        } finally {
            if (cursor != null) {
                cursor.close();
            }
            liteDatabase.close();
        }
    }

    /**
     * @GET Row Counter
     * */
    public int getCountsRows() {//Row counter
        SQLiteDatabase liteDatabase = dbHandler.getReadableDatabase();
        String query = "SELECT " + TABLE_ID + " FROM " + DELIVERY_ITEMS_TABLE;
        Cursor cursor = null;
        int count = 0;
        try{
            cursor = liteDatabase.rawQuery(query, null);
            if (cursor.getCount() < 0) {
                cursor.close();
            }
            if(cursor.moveToLast()) {
                cursor.moveToLast();
                count = cursor.getInt(cursor.getColumnIndexOrThrow(TABLE_ID));
            }
            return count;
        }finally {
            if (cursor != null) {
                cursor.close();
            }
            liteDatabase.close();
        }
    }
}
