package com.syntax.androidtechnicaltask.database.handler;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.syntax.androidtechnicaltask.database.dao.DeliveryItemDAO;

public class DbHandler extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "deliveryItemDB.db";

    public DbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        /*Creating Delivery Items Table*/
        String CREATE_DELIVERY_ITEMS_TABLE = "CREATE TABLE " + DeliveryItemDAO.DELIVERY_ITEMS_TABLE + "("
                + DeliveryItemDAO.TABLE_ID + " INTEGER PRIMARY KEY," + DeliveryItemDAO.ITEM_ID + " INT,"
                + DeliveryItemDAO.ITEM_DESCRIPTION + " TEXT,"
                + DeliveryItemDAO.ITEM_IMAGE_URL + " TEXT," + DeliveryItemDAO.ITEM_LATITUDE + " TEXT,"
                + DeliveryItemDAO.ITEM_LONGITUDE + " TEXT," + DeliveryItemDAO.ITEM_ADDRESS + " TEXT" + ")";
        db.execSQL(CREATE_DELIVERY_ITEMS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DeliveryItemDAO.DELIVERY_ITEMS_TABLE);
        onCreate(db);
    }
}
