package com.syntax.androidtechnicaltask.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;
import com.syntax.androidtechnicaltask.R;
import com.syntax.androidtechnicaltask.adapters.DeliveriesAdapter;
import com.syntax.androidtechnicaltask.database.dao.DeliveryItemDAO;
import com.syntax.androidtechnicaltask.interfaces.InternetConnectionContractor;
import com.syntax.androidtechnicaltask.listeners.EndlessRecyclerViewScrollListener;
import com.syntax.androidtechnicaltask.model.DeliveriesResponse;
import com.syntax.androidtechnicaltask.presenter.DeliveriesMainPresenter;
import com.syntax.androidtechnicaltask.utils.NetworkUtils;
import com.syntax.androidtechnicaltask.views.DeliveriesViewContractor;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements DeliveriesViewContractor.ViewDeliveries,
        DeliveriesAdapter.DeliveriesClickListener, InternetConnectionContractor.ViewInternetConnection {
    @BindView(R.id.recyclerView_deliveriesList)
    RecyclerView deliveriesListRV;
    @BindView(R.id.progressBar1)
    ProgressBar progressbar;

    private List<DeliveriesResponse> deliveriesList;
    private DeliveriesViewContractor.DeliveriesListPresenter deliveriesListPresenter;
    private DeliveriesAdapter deliveriesAdapter;
    private LinearLayoutManager linearLayoutManager;

    private int offset = 0;
    private int limit = 20;
    private int startNum = 0;
    private int endNum = limit;
    private int numberOfRows = 0;

    private DeliveryItemDAO deliveryItemDAO;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setTitle(R.string.delivery_items);
        deliveriesList = new ArrayList<>();
        deliveryItemDAO = new DeliveryItemDAO(this);

        setDeliveriesAdapter(deliveriesList);

        /*Register Internet Connection View*/
        NetworkUtils.getInstance().setViewInternetConnection(this);
        if (offset > 0) {
            //Do not Refresh
        } else {
            /*Register Deliveries Presenter*/
            deliveriesListPresenter = new DeliveriesMainPresenter(this);
            /*Connection Checking*/
            checkForConnection();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    /**
     * @INTERNET/NETWORK Connection
     */
    private void checkForConnection() {
        try {
            if (NetworkUtils.getInstance().isNetworkConnected(this)) {
                NetworkUtils.getInstance().checkForInternetConnection();
            } else {
                //No Network Connected
                snackBarBottomBar("No Network Connection");
                deliveriesList = deliveryItemDAO.getDeliveryItemsList(offset, limit);
                setDeliveriesAdapter(deliveriesList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onInternetConnection(boolean internet) {
        if (internet) {
            deliveriesListPresenter.requestDeliveries(offset, limit);
        } else {
            //No Internet connection
            snackBarBottomBar("No Internet Connection");
            startNum = offset * limit;
            endNum = (offset + 1) * limit;
            deliveriesList = deliveryItemDAO.getDeliveryItemsList(startNum, endNum);
            deliveriesAdapter.addItems(deliveriesList);
        }
    }

    @Override
    public void onError(String message) {
        progressbar.setVisibility(View.GONE);
        if (offset > 0)
            retryNext(message);
        else
            snackBarBottomBar(message);
    }

    @Override
    public void onDeliveryList(List<DeliveriesResponse> deliveriesList) {
        progressbar.setVisibility(View.GONE);
        if (deliveriesList != null) {
            int rowsFetched = deliveryItemDAO.getCountsRows();
            numberOfRows = (offset + 1) * limit;
            if (numberOfRows > rowsFetched || rowsFetched == 0) {
                for (DeliveriesResponse deliveriesResponse : deliveriesList) {
                    deliveryItemDAO.cacheDeliveryItems(deliveriesResponse);
                }
            }
            deliveriesAdapter.addItems(deliveriesList);
        }
    }

    @Override
    public void onShowLoader() {
        progressbar.setVisibility(View.VISIBLE);
    }

    /**
     * @param deliveriesList
     * @SET Adapter List
     */
    private void setDeliveriesAdapter(List<DeliveriesResponse> deliveriesList) {
        try {
            linearLayoutManager = new LinearLayoutManager(this);
            deliveriesAdapter = new DeliveriesAdapter(this, deliveriesList, this);
            deliveriesListRV.setHasFixedSize(true);
            deliveriesListRV.setLayoutManager(linearLayoutManager);
            deliveriesListRV.setAdapter(deliveriesAdapter);
            //Pagination here
            setPagination();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @PAGINATION With Endless ScrollView
     */
    private void setPagination() {
        try {
            EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    offset = page;
                    if (NetworkUtils.getInstance().isNetworkConnected(MainActivity.this)) {
                        NetworkUtils.getInstance().checkForInternetConnection();
                    } else {
                        //Fetch Rows from the DB
                        int rows = deliveryItemDAO.getCountsRows();
                        //STARTING INDEX
                        startNum = offset * limit;
                        //ENDING INDEX
                        endNum = (offset + 1) * limit;
                        //Fetch the Entries only when the rows of the DB equals to the Pagination End Numbers.
                        if (endNum <= rows) {
                            deliveriesList = deliveryItemDAO.getDeliveryItemsList(startNum, endNum);
                            if (!deliveriesList.isEmpty())
                                deliveriesAdapter.addItems(deliveriesList);
                        }
                    }
                }
            };
            deliveriesListRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDeliveryItemClicked(DeliveriesResponse deliveriesItem) {
        startActivity(new Intent(this, MapsActivity.class)
                .putExtra("DELIVERY_ITEM", deliveriesItem));
    }

    /**
     * @POP_ALERT shows at the bottom of the screen
     */
    private void snackBarBottomBar(String responseString) {
        try {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.root_layout), responseString, Snackbar.LENGTH_LONG);
            snackbar.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @POP_ALERT shows at the bottom of the screen
     */
    private void retryNext(String responseString) {
        try {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.root_layout), responseString, Snackbar.LENGTH_INDEFINITE)
                    .setAction("Retry", v -> deliveriesListPresenter.requestDeliveries(offset, limit));
            snackbar.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
