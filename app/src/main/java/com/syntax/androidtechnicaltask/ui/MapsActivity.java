package com.syntax.androidtechnicaltask.ui;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.syntax.androidtechnicaltask.R;
import com.syntax.androidtechnicaltask.model.DeliveriesResponse;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.layout_details)
    View cardView;
    @BindView(R.id.textView_description)
    TextView descriptionTxtV;
    @BindView(R.id.textView_address)
    TextView addressTxtV;
    @BindView(R.id.imageView_deliveryTo)
    ImageView deliverToImgV;

    private GoogleMap mMap;
    private DeliveriesResponse deliveryItem;
    private String itemDescription;
    private String itemAddress;
    private Double latitude;
    private Double longitude;
    private boolean isDetailShown = true;

    //Load animation
    private Animation slideDown;
    private Animation toolBarSlideDown;
    private Animation slideUp;
    private Animation toolBarSlideUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        /*Toolbar*/
        setToolbar();
        /*Get Bundles*/
        extractDeliveryItem();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        toolBarSlideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.toolbar_down);
        slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        toolBarSlideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.toolbar_up);
    }
    /**
     * SET Toolbar
     * */
    private void setToolbar(){
        toolbar.setTitle(getString(R.string.delivery_details));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }
    /**
     * Get the Parcelable Item
     * Delivery Item Details with Lat Long
     */
    private void extractDeliveryItem(){
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                deliveryItem = bundle.getParcelable("DELIVERY_ITEM");
                if (deliveryItem != null) {
                    itemDescription = deliveryItem.getDescription();
                    itemAddress = deliveryItem.getLocation().getAddress();
                    latitude = deliveryItem.getLocation().getLat();
                    longitude = deliveryItem.getLocation().getLng();

                    descriptionTxtV.setText(itemDescription);
                    addressTxtV.setText(itemAddress);
                    /**
                     * @IMAGE_LOADING
                     * It will load the Image from URL into an ImageView once it receive the image URL
                     * It also Cache the Image from the URL
                     * */
                    Glide.with(this)
                            .load(deliveryItem.getImageUrl())
                            .centerCrop()
                            .placeholder(R.mipmap.placeholder_image)
                            .into(deliverToImgV);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * We have add a marker on the position required by response (lat/lng).
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in the Required Address and move the camera
        LatLng itemDeliveryLocation = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(itemDeliveryLocation).title(itemAddress));//Title will be Address
        mMap.moveCamera(CameraUpdateFactory.newLatLng(itemDeliveryLocation));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(itemDeliveryLocation)      // Sets the center of the map to location user
                .zoom(17)                   // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                .tilt(40)                   // Sets the tilt of the camera to 40 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (isDetailShown){//Hide if the Details are already shown
                    toolbar.startAnimation(toolBarSlideUp);
                    cardView.startAnimation(slideDown);
                    toolbar.setVisibility(View.GONE);
                    cardView.setVisibility(View.GONE);
                    isDetailShown = false;
                } else {//Show if the Details are already hidden
                    toolbar.setVisibility(View.VISIBLE);
                    toolbar.startAnimation(toolBarSlideDown);
                    cardView.startAnimation(slideUp);
                    cardView.setVisibility(View.VISIBLE);
                    isDetailShown = true;
                }
            }
        });
    }
    /*Option for Backpress*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
