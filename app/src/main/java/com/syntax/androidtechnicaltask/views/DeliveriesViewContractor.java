package com.syntax.androidtechnicaltask.views;

import com.syntax.androidtechnicaltask.model.DeliveriesResponse;

import java.util.List;

public interface DeliveriesViewContractor {
    /**
     *
     * @FAILED_CASES Views
     */
    interface ViewFailedCases{
        void onError(String message);
    }

    /**
     * @DELIVERIES Views
     * */
    interface ViewDeliveries extends ViewFailedCases{
        void onDeliveryList(List<DeliveriesResponse> deliveriesList);
        void onShowLoader();
    }
    /**
     * @DELIVERIES Presenter
     * */
    interface DeliveriesListPresenter{
        void requestDeliveries(int offset, int limit);
    }
}
