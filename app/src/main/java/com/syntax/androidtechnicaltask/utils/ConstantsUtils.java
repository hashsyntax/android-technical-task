package com.syntax.androidtechnicaltask.utils;

public class ConstantsUtils {
    //Complete address :   https://mock-api-mobile.dev.lalamove.com/deliveries/?offset=0&limit=20
    public static final String BASE_URL = "https://mock-api-mobile.dev.lalamove.com";

    /*END POINTS*/
    public static final String DELIVERIES_ENDPOINT = "/deliveries?";
}
