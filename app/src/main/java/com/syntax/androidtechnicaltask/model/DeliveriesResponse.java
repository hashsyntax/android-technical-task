package com.syntax.androidtechnicaltask.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliveriesResponse implements Parcelable {
    @JsonProperty("id")
    private Integer itemId;
    @JsonProperty("description")
    private String description;
    @JsonProperty("imageUrl")
    private String imageUrl;
    @JsonProperty("location")
    private LocationLatLong location;

    public DeliveriesResponse() {
    }

    protected DeliveriesResponse(Parcel in) {
        if (in.readByte() == 0) {
            itemId = null;
        } else {
            itemId = in.readInt();
        }
        description = in.readString();
        imageUrl = in.readString();
        this.location = in.readParcelable(LocationLatLong.class.getClassLoader());
    }

    public static final Creator<DeliveriesResponse> CREATOR = new Creator<DeliveriesResponse>() {
        @Override
        public DeliveriesResponse createFromParcel(Parcel in) {
            return new DeliveriesResponse(in);
        }

        @Override
        public DeliveriesResponse[] newArray(int size) {
            return new DeliveriesResponse[size];
        }
    };

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public LocationLatLong getLocation() {
        return location;
    }

    public void setLocation(LocationLatLong location) {
        this.location = location;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (itemId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(itemId);
        }
        dest.writeString(description);
        dest.writeString(imageUrl);
        dest.writeParcelable(location, flags);
    }
}
