package com.syntax.androidtechnicaltask.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.syntax.androidtechnicaltask.R;
import com.syntax.androidtechnicaltask.model.DeliveriesResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DeliveriesAdapter extends RecyclerView.Adapter<DeliveriesAdapter.DeliveriesViewHolder> {
    private Context mContext;
    private List<DeliveriesResponse> deliveriesResponseList;
    private DeliveriesClickListener deliveriesClickListener;
    public DeliveriesAdapter(Context context, List<DeliveriesResponse> deliveriesResponseList, DeliveriesClickListener deliveriesClickListener) {
        this.mContext = context;
        this.deliveriesResponseList = deliveriesResponseList;
        this.deliveriesClickListener = deliveriesClickListener;
    }

    @NonNull
    @Override
    public DeliveriesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.deliveries_list_row_layout, viewGroup, false);
        return new DeliveriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DeliveriesViewHolder deliveriesViewHolder, int i) {
        try {
            final DeliveriesResponse deliveriesItem = deliveriesResponseList.get(i);
            deliveriesViewHolder.descriptionTxtV.setText(deliveriesItem.getDescription());
            deliveriesViewHolder.addressTxtV.setText(deliveriesItem.getLocation().getAddress());
            /**
             * @IMAGE_LOADING
             * It will load the Image from URL into an ImageView once it receive the image URL
             * It also Cache the Image from the URL
             * */
            Glide.with(mContext)
                    .load(deliveriesItem.getImageUrl())
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.mipmap.placeholder_image)
                    .into(deliveriesViewHolder.deliverToImgV);

            deliveriesViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deliveriesClickListener.onDeliveryItemClicked(deliveriesItem);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return deliveriesResponseList.size();
    }

    public void addItems(List<DeliveriesResponse> deliveriesList) {
        try {
            final int positionStart = deliveriesResponseList.size() + 1;
            deliveriesResponseList.addAll(deliveriesList);
            notifyItemRangeInserted(positionStart, deliveriesList.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class DeliveriesViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cardview)
        View cardView;
        @BindView(R.id.textView_description)
        TextView descriptionTxtV;
        @BindView(R.id.textView_address)
        TextView addressTxtV;
        @BindView(R.id.imageView_deliveryTo)
        ImageView deliverToImgV;

        DeliveriesViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    /**
     * @DELIVERIES Item Listener
     * */
    public interface DeliveriesClickListener {
        void onDeliveryItemClicked(DeliveriesResponse deliveriesResponse);
    }
}
